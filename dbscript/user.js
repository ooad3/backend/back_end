const mongoose = require('mongoose')
const User = require('../model/User')
const { ROLE } = require('../constant')
mongoose.connect('mongodb://localhost:27017/example')
async function clearUser () {
  await User.deleteMany({})
}

const main = async function () {
  await clearUser()

  const user = new User({ username: 'user@mail.com', password: 'password', roles: [ROLE.USER] })
  await user.save()
  const admin = new User({ username: 'admin@mail.com', password: 'password', roles: [ROLE.ADMIN, ROLE.USER] })
  await admin.save()
  const newUser = await User.findOne({ username: 'user', password: 'password' }, '-password').exec()
  console.log(newUser)
}

main().then(function () {
  console.log('Finish')
})
