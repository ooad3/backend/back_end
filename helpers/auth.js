const jwt = require('jsonwebtoken')
const User = require('../model/User')

const generateAccessToken = function (user) {
  return jwt.sign(user, process.env.TOKEN_SECRET, { expiresIn: '1d' })
}
const authenMiddleware = function (req, res, next) {
  const authHeader = req.headers.authorization
  const token = authHeader && authHeader.split(' ')[1]

  if (token === null) return res.sendStatus(401)
  jwt.verify(token, process.env.TOKEN_SECRET, async function (err, user) {
    console.log(err)
    if (err) return res.sendStatus(403)
    const currentUser = await User.findById(user._id).exec()
    req.user = currentUser
    next()
  })
}

const authorizeMiddleware = function (roles) {
  return async function (req, res, next) {
    console.log(req.user)
    const user = await User.findById(req.user._id).exec()
    console.log(user)
    for (let i = 0; i < roles.length; i++) {
      if (user.roles.indexOf(roles[i]) >= 0) {
        next()
        return
      }
    }
    res.sendStatus(401)
    console.log(req.user)
    next()
  }
}
module.exports = {
  generateAccessToken,
  authenMiddleware,
  authorizeMiddleware
}
