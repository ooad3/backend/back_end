const express = require('express')
const router = express.Router()
const User = require('../models/User')

const login = async function (req, res, next) {
  const username = req.body.username
  const password = req.body.password
  try {
    const user = await User.findOne({ username: username, password: password }, '-password').exec()
    if (user === null) {
      return res.status(404).json({
        message: 'User not found!!'
      })
    }
    res.json(user)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

router.post('/login', login) // Add New User

module.exports = router
